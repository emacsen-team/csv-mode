csv-mode (1.25-4) unstable; urgency=medium

  * Orphan package.

 -- Nicholas D Steeves <sten@debian.org>  Sat, 03 Aug 2024 18:50:22 -0400

csv-mode (1.25-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 17:52:25 +0900

csv-mode (1.25-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Jul 2024 21:37:17 +0900

csv-mode (1.25-1) unstable; urgency=medium

  * New upstream release.
  * Declare Standards-Version 4.7.0 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Fri, 21 Jun 2024 20:47:52 -0400

csv-mode (1.23-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas D Steeves <sten@debian.org>  Tue, 23 Jan 2024 14:47:38 -0500

csv-mode (1.22-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + elpa-csv-mode: Drop versioned constraint on emacs in Recommends.

  [ Nicholas D Steeves ]
  * Update upstream signing keys.  Yes, plural.  GNU ELPA now uses two, and
    both are required for signature verification.
  * New upstream release.
  * Update upstream copyright ranges (upstream and my own).
  * Declare Standards-Version 4.6.2. (no further changes required).

 -- Nicholas D Steeves <sten@debian.org>  Tue, 14 Feb 2023 16:18:02 -0500

csv-mode (1.21-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas D Steeves <sten@debian.org>  Mon, 27 Jun 2022 17:09:44 -0400

csv-mode (1.20-1) unstable; urgency=medium

  * New upstream release.
  * Update upstream copyright year range.

 -- Nicholas D Steeves <sten@debian.org>  Mon, 23 May 2022 18:19:42 -0400

csv-mode (1.19-1) unstable; urgency=medium

  * New upstream release.
  * Update my copyright range.

 -- Nicholas D Steeves <sten@debian.org>  Wed, 04 May 2022 12:13:45 -0400

csv-mode (1.18-1) unstable; urgency=medium

  * New upstream release.
  * Create elpa-test to set the backtrace truncate point to a large value to
    provide a more useful backtrace should any test fail.
  * Update my email address.
  * Migrate to debhelper-compat 13.
  * Declare Standards-Version 4.6.0. (no further changes required)

 -- Nicholas D Steeves <sten@debian.org>  Thu, 13 Jan 2022 16:30:15 -0500

csv-mode (1.12-1) unstable; urgency=medium

  * New upstream release.
  * Add upstream/signing-key.asc
  * Update watch file to use upstream uncompressed release tarball, download
    tarball signature, and finally compress the verified tarball with xz.
  * control: Add Rules-Requires-Root: no.
  * Enable autopkgtests for new upstream tests.
  * Declare Standards-Version 4.5.0. (no further changes required)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Wed, 19 Feb 2020 18:15:20 -0500

csv-mode (1.11-1) unstable; urgency=medium

  * New upstream version.
  * Copyright: Reflect upstream addition of 2020, and also update my
    copyright range.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 03 Feb 2020 14:20:13 -0700

csv-mode (1.10-1) unstable; urgency=medium

  * New upstream version.
  * Drop add-customisation-group-for-mode.patch (merged upstream)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Thu, 14 Nov 2019 16:00:18 -0500

csv-mode (1.9-2) unstable; urgency=medium

  * Update Upstream-Contact to "bug-gnu-emacs@gnu.org"; The upstream author
    requests that we report upstream bugs using "M-x report-emacs-bug RET",
    and submit patches to this address.
  * Update copyright date range for upstream files.
  * Update description to note newly featured support for TSV.
  * Declare Standards-Version 4.4.1. (no changes required)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 29 Sep 2019 17:15:57 -0400

csv-mode (1.9-1) unstable; urgency=medium

  * Import Upstream version 1.9
  * Rebase add-customisation-group-for-mode.patch onto upstream/1.9

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sat, 28 Sep 2019 21:09:30 -0400

csv-mode (1.8-1) unstable; urgency=medium

  * New upstream version.
  * Update status of add-customisation-group-for-mode.patch to
    "Forwarded: yes"

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 23 Sep 2019 17:28:57 -0400

csv-mode (1.7-2) unstable; urgency=medium

  * Switch to debhelper-compat 12.
  * Drop Enhances: emacs25 (it is a dummy transitional package)
  * Update my copyright years.
  * Add add-customisation-group-for-mode.patch.  M-x customize-mode now
    displays the correct group.
  * Declare Standards-Version 4.4.0. (No changes required)
  * Rebuild with current dh-elpa.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 26 Aug 2019 18:13:14 -0400

csv-mode (1.7-1) unstable; urgency=medium

  * Initial release. (Closes: #902398)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Tue, 26 Jun 2018 12:57:14 -0400
